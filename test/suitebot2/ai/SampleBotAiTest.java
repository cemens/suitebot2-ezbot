package suitebot2.ai;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import suitebot2.game.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class SampleBotAiTest
{
	public static final int MY_ID = 1;
	public static final ImmutableList<Player> PLAYERS = ImmutableList.of(
			new Player(1, "Me"), new Player(2, "Enemy 1"), new Player(3, "Enemy 2"));
	public static final ImmutableList<Point> STARTING_POSITIONS = ImmutableList.of(
			new Point(1, 1), new Point(2, 2), new Point(3, 3));
	public static final int PLAN_WIDTH = 10;
	public static final int PLAN_HEIGHT = 5;
	public static final int MAX_ROUNDS = 4;

	public static final GamePlan GAME_PLAN = new GamePlan(PLAN_WIDTH, PLAN_HEIGHT, STARTING_POSITIONS, MAX_ROUNDS);
	public static final GameSetup GAME_SETUP = new GameSetup(MY_ID, PLAYERS, GAME_PLAN);

	private BotAi botAi = new SampleBotAi();

	@Test
	public void shouldFireIfEnemyFired() throws Exception
	{
		botAi.initializeAndMakeMove(GAME_SETUP);
		String myMoveAfterEnemyFired = botAi.makeMove(makeGameRound("MY MOVE", "FIRE", "WARM UP"));
		assertThat(myMoveAfterEnemyFired, is("FIRE"));
	}

	@Test
	public void shouldNotFireIfEnemyDidNotFire() throws Exception
	{
		String myFirstMove = botAi.initializeAndMakeMove(GAME_SETUP);
		assertThat(myFirstMove, is(not("FIRE")));
		String myMoveAfterEnemyDidNotFire = botAi.makeMove(makeGameRound("MY MOVE", "WARM UP", "WARM UP"));
		assertThat(myMoveAfterEnemyDidNotFire, is(not("FIRE")));
	}

	@Test
	public void shouldWarmUpInFirstHalf() throws Exception
	{
		GameRound gameRound = makeGameRound("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE");
		String myRound1Move = botAi.initializeAndMakeMove(GAME_SETUP);
		assertThat(myRound1Move, is("WARM UP"));
		String myRound2Move = botAi.makeMove(gameRound);
		assertThat(myRound2Move, is("WARM UP"));
	}

	@Test
	public void shouldNoLongerWarmUpInSecondHalf() throws Exception
	{
		GameRound gameRound = makeGameRound("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE");
		botAi.initializeAndMakeMove(GAME_SETUP);
		botAi.makeMove(gameRound);
		String myRound3Move = botAi.makeMove(gameRound);
		assertThat(myRound3Move, is(not("WARM UP")));
		String myRound4Move = botAi.makeMove(gameRound);
		assertThat(myRound4Move, is(not("WARM UP")));
	}

	private GameRound makeGameRound(String... moves)
	{
		return new GameRound(IntStream.range(0, PLAYERS.size())
		                              .mapToObj(i -> new PlayerMove(PLAYERS.get(i).id, moves[i]))
		                              .collect(Collectors.toList()));
	}
}