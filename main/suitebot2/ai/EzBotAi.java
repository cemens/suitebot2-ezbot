package suitebot2.ai;

import suitebot2.game.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EzBotAi implements BotAi
{
	private int myId;
	private List<Player> players;
	private Map<Integer, Point> playerPositions;
	private GamePlan gamePlan;
	private int round;
	private int board[][];
	private int boardWidth;
	private int boardHeight;

	int[][] weights = new int[][]
	{
		{25,20,15,10,5},
		{20,16,12,8,4},
		{15,12,9,6,3},
		{10,8,6,4,2},
		{5,4,3,2,1},
	};



	public static void main(String[] args) {

		List<Player> players = new ArrayList<Player>();
		List<Point> points = new ArrayList<Point>();
		for (int i=0; i<18; i++)
		{
			int x = i % 6;
			int y = i / 6;
			players.add(new Player(100+i, "player " + i));
			points.add(new Point(x * 3 + 3, y*6 +6));
		}
		GamePlan gamePlan = new GamePlan(90, 60, points, 250);

		GameSetup gameSetup = new GameSetup(players.get(0).id, players, gamePlan);

		EzBotAi  bot = new EzBotAi();
		String move = bot.initializeAndMakeMove(gameSetup);
		System.out.println(move);


		for (int i=0; i<250; i++)
		{
			ArrayList playerMoves = new ArrayList<PlayerMove>();
			for (int playIndex=0; playIndex<18; playIndex++)
			{
				PlayerMove m = new PlayerMove(players.get(playIndex).id, "U");
				playerMoves.add(m);
			}
			GameRound gameRound = new GameRound(playerMoves);

			String myMove = bot.makeMove(gameRound);
			System.out.println(myMove);
		}

	}

	@Override
	public String initializeAndMakeMove(GameSetup gameSetup)
	{
		myId = gameSetup.aiPlayerId;
		gamePlan = gameSetup.gamePlan;
		players = gameSetup.players;
		round = 1;

		board = new int[gameSetup.gamePlan.width][gameSetup.gamePlan.height];

		for (int i=0; i<gameSetup.gamePlan.width; i++)
			for (int j=0; j<gameSetup.gamePlan.height; j++)
				board[i][j] = -1;

//		board[3][5] = 1;
//		board[2][6] = 1;

		int index = 0;
		for (Point p : gameSetup.gamePlan.startingPositions)
		{
			board[p.x][p.y] = index ++;
		}

		boardWidth = gameSetup.gamePlan.width;
		boardHeight = gameSetup.gamePlan.height;

		playerPositions = new HashMap<Integer, Point>();
		for (int i=0; i<gameSetup.gamePlan.startingPositions.size(); i++)
		{
			Point p = gameSetup.gamePlan.startingPositions.get(i);
			int playerId = players.get(i).id;
			playerPositions.put(playerId, new Point(p.x, p.y));
		}


		return figureOutMove();
	}


	String[] startingMoves = new String[] {
			"R","R","R",
			"U","U","U",
			"L","L","L","L","L","L",
			"D","D","D","D","D","D",
			"R","R","R","R","R","R",
			"U","U",

			"R","R","R",
			"U","U","U","U",
			"L","L",

			"U","U","U",
			"L","L","L",
			"D","D",

			"L","L","L","L",
			"U","U","U",
			"R","R","R","R",


	};

	@Override
	public String makeMove(GameRound gameRound)
	{
		round++;

		for (PlayerMove playerMove : gameRound.moves)
			simulateMove(playerMove);

		return figureOutMove();
	}

	private void simulateMove(PlayerMove playerMove)
	{
		Point p = playerPositions.get(playerMove.playerId);
		Point newPos;

		if ("U".equals(playerMove.move))
		{
			newPos = new Point(p.x, modY(p.y - 1));
		}
		else if ("D".equals(playerMove.move))
		{
			newPos = new Point(p.x, modY(p.y + 1));
		}
		else if ("L".equals(playerMove.move))
		{
			newPos = new Point(modX(p.x - 1), p.y);
		}
		else if ("R".equals(playerMove.move))
		{
			newPos = new Point(modX(p.x + 1), p.y);
		}
		else
		{
			newPos = new Point(p.x, p.y);
		}

		playerPositions.put(playerMove.playerId, newPos);

		board[newPos.x][newPos.y] = playerMove.playerId;
	}

	private String figureOutMove()
	{
//		dumpBoard();

		if (round-1 < startingMoves.length)
			return startingMoves[round-1];
		else
		{
			// todo
			Point myPos = playerPositions.get(myId);
			int x = myPos.x;
			int y = myPos.y;

			int R = 0, L=0, U=0, D=0;

			for (int dx=-4; dx<=4; dx++)
				for (int dy=-4; dy<=4; dy++)
				{
					int weight = weights[Math.abs(dx)][Math.abs(dy)];

					if (board[modX(x+dx)][modY(y+dy)] == -1)
					{
						if (dx < 0) L += weight;
						if (dx > 0) R += weight;
						if (dy < 0) U += weight;
						if (dy > 0) D += weight;
					}
				}

//			int lookAhead = 5;
//			for (int i=1; i<lookAhead; i++)
//			{
//				if (board[modX(x + i)][y] == -1)
//				{
//					R += weight(i);
//				}
//				else if (board[modX(x - i)][y] == -1)
//				{
//					L += weight(i);
//				}
//				else if (board[x][modY(y - i)] == -1)
//				{
//					U += weight(i);
//				}
//				else if (board[x][modY(y + i)] == -1)
//				{
//					D += weight(i);
//				}
//
//			}

			if (R>=L && R>=U && R>=D)
				return "R";
			else if (L>=U && L>=D)
				return "L";
			else if (U>=D)
				return "U";
			else
				return "D";

		}
	}

	int weight(int i)
	{
		return i>1 ? 1 : 2;
	}
	int modX(int x)
	{
		return (x+boardWidth) % boardWidth;
	}

	int modY(int x)
	{
		return (x+boardHeight) % boardHeight;
	}

	void dumpBoard()
	{
		Point myPos = playerPositions.get(myId);
		int x = myPos.x;
		int y = myPos.y;
		System.out.println(x + ", " +y);

		for (int j=0; j<boardHeight; j++)
		{
			for (int i=0; i<boardWidth; i++)
			{
				System.out.print(board[i][j] == -1 ? "." : "*");
			}
			System.out.println();
		}

	}
}
